﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Edge : MonoBehaviour {
    public GameObject Respawn;
    public GameObject Character;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.transform.position = Respawn.transform.position;
            other.gameObject.GetComponent<Character>().hp = 100;
        }
        else
            Destroy(other.gameObject);
    }
}