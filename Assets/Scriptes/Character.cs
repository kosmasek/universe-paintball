﻿using System.Collections;
using UnityEngine;

public class Character : MonoBehaviour {

    [SerializeField]
    private float speed = 3.0f;
    [SerializeField]
    public int hp = 100;
    [SerializeField]
    private float jumpforce = 15.0f;

    private bool isGrounded = false;

    new private Rigidbody2D rigidbody;
    private Animator animator;
    private SpriteRenderer sprite;
    public Edge CurrentEdge;



    private CharState State
    {
        get { return (CharState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }

    private void Awake()
    {
        rigidbody = GetComponent < Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
     }

    private void FixedUpdate()
    {
        CheckGround();
    }

    private void Update()
    {
        

        if (isGrounded)
            State = CharState.Idle;

        if (Input.GetButton("Horizontal"))
            Run();

        if (Input.GetButtonDown("Jump") && isGrounded)
            Jump();

        if (hp <= 0)
        {
            transform.position = CurrentEdge.Respawn.transform.position;
            hp = 100;
        }


    }

    private void Run()
    {
        Vector3 direction = transform.right * Input.GetAxis("Horizontal");

        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);

        if (isGrounded) State = CharState.Run;
    }

    private void Jump()
    {
        rigidbody.AddForce(transform.up * jumpforce, ForceMode2D.Impulse);

        State = CharState.Jump;
    }

    private void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3f);

        isGrounded = colliders.Length > 1;

        if (!isGrounded) State = CharState.Jump;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Movable Destroyable Obstacle")
            hp -= 10;
        Debug.Log(hp);
     }

}

public enum CharState { 
    Idle,
    Run,
    Jump
}