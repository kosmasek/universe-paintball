﻿using UnityEngine;
using System.Collections;

public class FireScript2D : MonoBehaviour
{

    public float speed = 10; 
    public Rigidbody2D bullet; 
    public Transform gunPoint; 
    public float fireRate = 1;
    public bool FaceRight = true; 

    public Transform zRotate; 

    public float minAngle = -40;
    public float maxAngle = 40;

    private float curTimeout, angle;
    private int invert;
    private Vector3 mouse;

    void Start()
    {
        if (!FaceRight) invert = -1; else invert = 1;
    }


    private void Flip() 
    {
        FaceRight = !FaceRight;
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * -1, transform.localScale.z);
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Fire();
        }
        else
        {
            curTimeout = 1000;
        }
        var mousePosition = Input.mousePosition;

        if ((mousePosition.x > Screen.width / 2f && !FaceRight) ||
            (mousePosition.x < Screen.width / 2f && FaceRight))
        {
            Flip();
        }

        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        var angle = Vector2.Angle(Vector2.right, mousePosition - transform.position);
        transform.eulerAngles = new Vector3(0f, 0f, transform.position.y < mousePosition.y ? angle : -angle);
    }

    void Fire()
    {
        curTimeout += Time.deltaTime;
        if (curTimeout > fireRate)
        {
            curTimeout = 0;
            Vector3 direction = gunPoint.localPosition;
                     
            Rigidbody2D clone = Instantiate(bullet, gunPoint.position, Quaternion.identity) as Rigidbody2D;
            clone.velocity = transform.TransformDirection(direction.normalized * speed);
            clone.transform.right = direction.normalized;
            Destroy(clone.gameObject, 2f);
        }
    }
}