﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprite : MonoBehaviour {
    private bool FaceRight = true;

    private void Flip()
    {
        FaceRight = !FaceRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    // Update is called once per frame
    void Update () {
        var mousePosition = Input.mousePosition;

        if ((mousePosition.x > Screen.width / 2f && !FaceRight) ||
            (mousePosition.x < Screen.width / 2f && FaceRight))
            Flip();
    }
}
