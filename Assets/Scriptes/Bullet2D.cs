﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]

public class Bullet2D : MonoBehaviour
{


    void OnCollisionEnter2D(Collision2D coll)
    {
        switch (coll.gameObject.tag)
        {
            case "Movable Destroyable Obstacle":
                //уничтожение пули
                Destroy(gameObject);
                //уничтожение платформы
                Destroy(coll.gameObject);
                break;
            case "Obstacle":
                Destroy(gameObject);
                break;
            case "Enemy_2":
                break;
        }

        Destroy(gameObject);
    }
}